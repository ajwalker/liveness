# liveness

A container masquerading as an interesting service.

It can be used as a test service for health probes, or as:

- a basic internet relay chat service, supporting at most one participant.

    ```sh
    ./liveness client --data "hey" 127.0.0.1
    ```

- a datastore that throws everything away:

    ```sh
    ./liveness client --header "X-Status: 201" --data "store=value" 127.0.0.1
    ```

- an egg timer service:

    ```sh
    ./liveness client "127.0.0.1?timeout=3m" && echo eggs done!
    ```

## Usage

`liveness` includes both a client and server.

### client

```sh
$ ./liveness client --help
Usage: liveness client [options...] <url>
  -header value
        request header line
  -data string
        request data
```

### server

```sh
$ ./liveness server --help
Usage: liveness server
  -addr value
        address to listen on (default: ":80")
  -delay duration
        the startup delay before the daemon listens
```

## Example

```sh
# liveness won't start listening on any port until after the startup delay duration specified.

./liveness server --delay 5s --addr :80 --addr :8333
```

```sh
# An X-Status header sets the status to be returned.
# Any data passed is echoed back.
# A timeout query will delay answering the request for that duration.

$ ./liveness client --header "X-Status: 404" --data "hello" "127.0.0.1:8333/?timeout=5s"
HTTP/1.1 404 Not Found
Content-Length: 5
Content-Type: text/plain; charset=utf-8
Date: Tue, 30 Jun 2020 22:43:48 GMT
hello
```

```sh
# liveness logs everything that occurs

2020/06/30 23:43:31 delayed by 5s...
2020/06/30 23:43:36 listening :8333...
2020/06/30 23:43:36 listening :80...
2020/06/30 23:43:43 "127.0.0.1:54339" waiting: 5s
2020/06/30 23:43:48 "127.0.0.1:54339" replying with status code: 404
```