package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"
)

type list []string

func (v *list) String() string {
	return strings.Join(*v, ", ")
}

func (v *list) Set(addr string) error {
	*v = append(*v, addr)
	return nil
}

func main() {
	help := func() {
		fmt.Println("liveness subcommands are:")
		fmt.Println(" client")
		fmt.Println(" server")
		os.Exit(1)
	}

	if len(os.Args) < 2 {
		help()
	}

	switch os.Args[1] {
	case "client":
		client()
	case "server":
		server()
	default:
		help()
	}
}

func client() {
	var (
		flags   = flag.NewFlagSet("client", flag.ExitOnError)
		headers list
		data    string
	)

	flags.Usage = func() {
		fmt.Fprintf(flags.Output(), "Usage: liveness client [options...] <url>\n")
		flags.PrintDefaults()
		os.Exit(1)
	}

	flags.Var(&headers, "header", "request header line")
	flags.StringVar(&data, "data", "", "request data")
	flags.Parse(os.Args[2:])

	exitOnError := func(err error) {
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}

	if flags.NArg() < 1 {
		flags.Usage()
	}

	uri := flags.Args()[0]
	if !strings.HasPrefix(uri, "http:") && !strings.HasPrefix(uri, "https:") {
		uri = "http://" + uri
	}

	u, err := url.Parse(uri)
	exitOnError(err)

	req, _ := http.NewRequest(http.MethodGet, u.String(), bytes.NewReader([]byte(data)))
	for _, hdr := range headers {
		var kv [2]string
		copy(kv[:], strings.SplitN(hdr, ":", 2))
		req.Header.Add(kv[0], kv[1])
	}

	resp, err := http.DefaultClient.Do(req)
	exitOnError(err)

	fmt.Printf("%s %s\n", resp.Proto, resp.Status)
	resp.Header.Write(os.Stderr)

	defer resp.Body.Close()
	_, err = io.Copy(os.Stdout, resp.Body)
	exitOnError(err)

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		os.Exit(1)
	}
}

func server() {
	var (
		flags = flag.NewFlagSet("server", flag.ExitOnError)
		addrs list
		delay time.Duration
	)

	flags.Usage = func() {
		fmt.Fprintf(flags.Output(), "Usage: liveness server\n")
		flags.PrintDefaults()
		os.Exit(1)
	}

	flags.Var(&addrs, "addr", "address to listen on (default: \":80\")")
	flags.DurationVar(&delay, "delay", 0, "the startup delay before the daemon listens")
	flags.Parse(os.Args[2:])

	if len(addrs) == 0 {
		addrs = append(addrs, ":80")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// X-Status header allows for controlling HTTP return status
		status, _ := strconv.ParseInt(r.Header.Get("X-Status"), 10, 64)
		if status == 0 {
			status = http.StatusOK
		}

		// timeout query allows for controlling time it takes to respond
		if timeout := r.URL.Query().Get("timeout"); timeout != "" {
			sleep, err := time.ParseDuration(timeout)
			if err != nil {
				log.Printf("%q error parsing timeout: %v", r.RemoteAddr, err)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			log.Printf("%q waiting: %v", r.RemoteAddr, sleep)
			time.Sleep(sleep)
		}

		// echo
		w.WriteHeader(int(status))
		defer r.Body.Close()
		_, err := io.Copy(w, r.Body)
		if err != nil {
			log.Printf("%q error writing: %v", r.RemoteAddr, err)
			return
		}

		log.Printf("%q replying with status code: %d", r.RemoteAddr, status)
	})

	// listen after initial startup delay
	log.Printf("delayed by %s...", delay)
	time.AfterFunc(delay, func() {
		for _, addr := range addrs {
			go func(addr string) {
				log.Printf("listening %s...", addr)
				panic(http.ListenAndServe(addr, nil))
			}(addr)
		}
	})

	// block forever
	select {}
}
