VERSION = 0.1.0
CI_COMMIT_SHORT_SHA ?= dev
REPO = registry.gitlab.com/ajwalker/liveness

LINUX_PLATFORMS += amd64
LINUX_PLATFORMS += 386
LINUX_PLATFORMS += s390x
LINUX_PLATFORMS += ppc64le
LINUX_PLATFORMS += arm-v6
LINUX_PLATFORMS += arm-v7
LINUX_PLATFORMS += arm64-v8

WINDOWS_PLATFORMS += nanoserver-1809

build-linux:
	@ make $(foreach PLATFORM,$(LINUX_PLATFORMS),platform-linux-$(PLATFORM))

build-windows:
	@ make $(foreach PLATFORM,$(WINDOWS_PLATFORMS),platform-windows-$(PLATFORM))

platform-linux-%:
	@ docker buildx build \
		--platform $(subst -,/,$(subst platform-,,$@)) \
		-t $(REPO):$(VERSION)-$(subst -,,$(subst linux-,,$(subst platform-,,$@)))-$(CI_COMMIT_SHORT_SHA) .

platform-windows-%:
	@ docker build -f Dockerfile.windows --build-arg=TAG=$(subst platform-windows-nanoserver-,,$@) \
		-t $(REPO):$(VERSION)-$(subst platform-windows-,,$@)-$(CI_COMMIT_SHORT_SHA) .

push:
	@ docker push ${REPO}

push-multiarch:
	@ docker manifest create --amend ${REPO}:${VERSION} $(foreach PLATFORM,$(LINUX_PLATFORMS),${REPO}:${VERSION}-$(subst -,,$(PLATFORM))-$(CI_COMMIT_SHORT_SHA))
	@ docker manifest create --amend ${REPO}:${VERSION} $(foreach PLATFORM,$(WINDOWS_PLATFORMS),${REPO}:${VERSION}-$(PLATFORM)-$(CI_COMMIT_SHORT_SHA))
	@ docker manifest push --purge ${REPO}:${VERSION}